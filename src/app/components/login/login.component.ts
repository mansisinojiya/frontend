import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {ApiService} from "../../api.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private formBuilder: FormBuilder, private apiService: ApiService, private toastr: ToastrService) {
    }
    loginForm: FormGroup;
    email = '';
    password = '';
    submitted = false;
    private StorageData: any = {};


    get loginControl() {
        return this.loginForm.controls;
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: ['', [
                Validators.required,
                Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)
            ]],
            password: ['', [Validators.required]],
        });
    }

    // On submit button click
    onSubmit() {
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }
        const loginData = {
            email: this.loginForm.value.email,
            password: this.loginForm.value.password,
        };
        this.apiService.login(loginData).subscribe((data: any) => {
          console.log(data);
          setTimeout(() => {
                if (data.data) {
                    this.StorageData = {
                        data  : data.data,
                        token : data.meta.token,
                    };
                    localStorage.setItem('loginData', JSON.stringify(this.StorageData));
                    this.router.navigate(['dashboard']);
                    this.toastr.success(data.meta.message);
                }
            },   500 );
        }, err => {
            this.toastr.error(err.error.message, 'Error');
        });
    }

}
