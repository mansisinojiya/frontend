import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {ApiService} from "../../api.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router, private formBuilder: FormBuilder, private apiService: ApiService, private toastr: ToastrService) {
    }
    dashboardForm: FormGroup;
    email = '';
    password = '';
    submitted = false;
    private StorageData: any = {};


    get dashboardControl() {
        return this.dashboardForm.controls;
    }

    ngOnInit() {
        this.dashboardForm = this.formBuilder.group({
            email: ['', [
                Validators.required,
                Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)
            ]],
            password: ['', [Validators.required]],
        });
    }

    // On submit button click
    onSubmit() {
        this.submitted = true;
        if (this.dashboardForm.invalid) {
            return;
        }
        const dashboardData = {
            email: this.dashboardForm.value.email,
            password: this.dashboardForm.value.password,
        };
        this.apiService.login(dashboardData).subscribe((data: any) => {
          console.log(data);
          setTimeout(() => {
                if (data.data) {
                    this.StorageData = {
                        data  : data.data,
                        token : data.meta.token,
                    };
                    localStorage.setItem('dashboardData', JSON.stringify(this.StorageData));
                    // this.router.navigate(['dashboard']);
                    this.toastr.success(data.meta.message);
                }
            },   500 );
        }, err => {
            this.toastr.error(err.error.message, 'Error');
        });
    }

}
