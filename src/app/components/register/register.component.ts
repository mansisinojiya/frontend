import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {ApiService} from "../../api.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router, private formBuilder: FormBuilder, private apiService: ApiService, private toastr: ToastrService) {
    }
    registerForm: FormGroup;
    submitted = false;
    private StorageData: any = {};


    get registerControl() {
        return this.registerForm.controls;
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            email: ['', [
                Validators.required,
                Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)
            ]],
            password: ['', [Validators.required]],
            firstname: ['', [Validators.required]],
            lastname: ['', [Validators.required]],
            mobile: ['', [Validators.required]],
            pincode: ['', [Validators.required]],
            city_id: ['', [Validators.required]],
            hobby_id: ['', [Validators.required]],
            gender: ['', [Validators.required]],
        });
    }

    // On submit button click
    onSubmit() {
        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }
        const registerData = {
            email: this.registerForm.value.email,
            password: this.registerForm.value.password,
            firstname: this.registerForm.value.firstname,
            lastname: this.registerForm.value.lastname,
            gender: this.registerForm.value.gender,
            city_id: this.registerForm.value.city_id,
            hobby_id: this.registerForm.value.hobby_id,
            mobile: this.registerForm.value.mobile,
            pincode: this.registerForm.value.password,
        };
        this.apiService.register(registerData).subscribe((data: any) => {
          console.log(data);
          setTimeout(() => {
                if (data.data) {
                    this.StorageData = {
                        data  : data.data,
                        token : data.meta.token,
                    };
                    localStorage.setItem('registerData', JSON.stringify(this.StorageData));
                    // this.router.navigate(['dashboard']);
                    this.toastr.success(data.meta.message);
                }
            },   500 );
        }, err => {
            this.toastr.error(err.error.message, 'Error');
        });
    }

}
