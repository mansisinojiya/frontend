import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {environment} from '../environments/environment';
import 'rxjs/Rx';

@Injectable({
    providedIn: 'root'
})

export class ApiService {

    baseUrl: string = environment.apiUrl;
    private httpOptions: any;
    private loginData: any;

    public static isAuthenticated(): boolean {
        const token = JSON.parse(localStorage.getItem('loginData'));
        if (token) {
            return true;
        }
        return false;
    }

    constructor(private http: HttpClient,
                private router: Router,
                private toastr: ToastrService) {
    }

    canActivate(){
        if (!ApiService.isAuthenticated()) {
          this.router.navigate(['login']);
        }
        this.router.navigate(['dashboard']);
    }

    getToken() {
        this.loginData = JSON.parse(localStorage.getItem('loginData'));
        this.httpOptions = {
            headers: new HttpHeaders({
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.loginData.token,
            }),
        };
        return this.httpOptions;
    }

    public static logout(){
        localStorage.removeItem('loginData');
        //this.router.navigate(['login']);
        return true;
    }

    private handleAuthError(err: HttpErrorResponse): Observable<any> {
        if (err.status === 401) {
            localStorage.removeItem('loginData');
            this.router.navigate(['login']);
        }
        return Observable.throw(err.error.message);
    }

    login(data: { email: any; password: any; }): Observable<any> {
        return this.http.post(this.baseUrl + 'login', data);
    }

    register(data: { email: any; password: any; }): Observable<any> {
        return this.http.post(this.baseUrl + 'register', data);
    }

}
