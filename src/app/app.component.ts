import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from './api.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router: Router,  private apiService: ApiService, private toastr: ToastrService) {
  }
  title = 'Angular test';
 
  ngOnInit() {
    this.canActivate();
  }
  canActivate(){
    if (!ApiService.isAuthenticated()) {
      this.router.navigate(['login']);
    }
    this.router.navigate(['dashboard']);
}
logout(){
  ApiService.logout();
  this.router.navigate(['login']);
}
}
